from wsgiref import simple_server

from schemas.patient import Patient, PatientSchema
from lib import *
import settings

app = falcon.API()

engine, meta, session = connect_with_session(**settings.POSTGRESQL)


def add_route(route: str):
    def decorate(cls):
        app.add_route(route, cls())
        return cls
    return decorate


@add_route('/patients/{patient_id:int}')
class PatientsResource:
    schema = PatientSchema()

    @json_body_resp
    def on_get(self, req: falcon.Request, resp: falcon.Response, patient_id: int):
        patient = get_by_id(session, Patient, patient_id)

        if not patient:
            resp.status = falcon.HTTP_404  # 404 - Not Found
            resp.body = "Patient with ID %d not found" % patient_id
            return

        print("GET Patient:", patient)
        resp.body = deserialize(self.schema, patient)

    @json_req
    @json_body_resp
    def on_patch(self, req: falcon.Request, resp: falcon.Response, patient_id: int):
        patient = get_by_id(session, Patient, patient_id)
        if patient:
            raw_data = req.context['json']

            valid = serialize(self.schema, raw_data, session)

            if valid:
                for key in valid:
                    setattr(patient, key, valid[key])

                try:
                    session.commit()
                    resp.body = deserialize(self.schema, patient)

                except sa.exc.IntegrityError:
                    session.rollback()
                    resp.body = {"error": "Email is not unique"}
                    resp.status = falcon.HTTP_400

            else:
                resp.body = {"error": "Field(s) is incorrect"}
                resp.status = falcon.HTTP_400

        else:
            resp.body = {"error": "Patient with ID %d does not exist" % patient_id}
            resp.status = falcon.HTTP_400

    @json_body_resp
    def on_delete(self, req: falcon.Request, resp: falcon.Response, patient_id: int):
        patient = get_by_id(session, Patient, patient_id)

        if patient:
            session.delete(patient)
            session.commit()
            resp.body = deserialize(self.schema, patient)

        else:
            resp.body = {"error": "Patient with ID %d does not exist" % patient_id}
            resp.status = falcon.HTTP_400


@add_route('/patients')
class PatientsCollection:
    schema = PatientSchema()

    @json_body_resp
    def on_get(self, req: falcon.Request, resp: falcon.Response):
        res = deserialize_all(
            self.schema,
            *query(session, Patient)
        )
        resp.body = res

    @json_req
    @json_body_resp
    def on_post(self, req: falcon.Request, resp: falcon.Response):
        def err(msg: str, status=falcon.HTTP_400):
            session.rollback()
            resp.body = {"error": msg}
            # print("ERROR:", msg, "(%s)" % status, file=sys.stderr)
            resp.status = status

        data = req.context['json']

        try:
            patient = serialize(self.schema, data, session)

            session.add(patient)
            session.commit()

        except sa.exc.IntegrityError:  # Пациент уже был создан
            return err("A patient with this data has already been created")

        except sa.orm.exc.UnmappedInstanceError:  # Поле(я) не корректны
            return err("Field(s) is incorrect")

        print("New Patient!", patient)
        resp.body = deserialize(self.schema, patient)


if __name__ == '__main__':
    svr = simple_server.make_server('0.0.0.0', 80, app)
    print('server start')
    svr.serve_forever()
