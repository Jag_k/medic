from email.utils import parseaddr

from datetime import date

from marshmallow_sqlalchemy import ModelSchema
from marshmallow_sqlalchemy.fields import fields

from models.patient import Patient


class PatientSchema(ModelSchema):
    class Meta:
        model = Patient

    id = fields.Integer(dump_only=True)

    email = fields.String(required=True,
                          validate=lambda email: '@' in parseaddr(email)[1])

    birthday = fields.Date(validate=lambda x: x <= date.today())
    passport_date = fields.Date(validate=lambda x: x < date.today())

    passport_number = fields.Integer(validate=lambda x: len(str(x)) == 4)
    passport_series = fields.Integer(validate=lambda x: len(str(x)) == 6)
