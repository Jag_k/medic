from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Date, String

from lib import connect
from settings import POSTGRESQL

engine, meta = connect(**POSTGRESQL)
Base = declarative_base()


class Patient(Base):
    __tablename__ = 'patients'

    id = Column(Integer, primary_key=True, unique=True)
    birthday = Column(Date)
    full_name = Column(String, nullable=False)
    email = Column(String, unique=True, nullable=False)

    passport_series = Column(Integer)
    passport_number = Column(Integer)
    passport_date = Column(Date)
    passport_issued = Column(String)

    address = Column(String)

    def __repr__(self):
        return '<Patient(id={self.id!r}, name={self.full_name!r})>'.format(self=self)


Base.metadata.create_all(engine)
