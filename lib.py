import sqlalchemy as sa
from sqlalchemy.orm import sessionmaker
import falcon


def connect(user, password, db, host='localhost', port=5432):
    """Returns a connection and a metadata object"""
    url = 'postgresql://{}:{}@{}:{}/{}'
    url = url.format(user, password, host, port, db)

    # The return value of create_engine() is our connection object
    engine = sa.create_engine(url, client_encoding='utf8')

    # We then bind the connection to MetaData()
    meta = sa.MetaData(bind=engine)
    meta.reflect()

    return engine, meta


def connect_with_session(user, password, db, host='localhost', port=5432) -> (sa.engine.Engine, sa.MetaData,
                                                                              sa.orm.session):
    engine, meta = connect(user, password, db, host, port)
    session = sessionmaker(bind=engine)()
    return engine, meta, session


def query(session, query, **kwargs):
    res = session.query(query).filter_by(**kwargs).all() \
        if kwargs else \
        session.query(query).all()
    for row in res:
        print(row)
    return res


def get_by_id(session, query, id):
    return session.query(query).get(id)


def deserialize(schema, data):
    return schema.dump(data).data


def deserialize_all(schema, *data):
    return [schema.dump(d).data for d in data]


def serialize(schema, data, session):
    return schema.load(data, session=session).data


def json_req(func):
    def decorate(self, req, resp, *args, **kwargs):
        try:
            data = falcon.json.load(req.bounded_stream)
            req.context['json'] = data
            return func(self, req, resp, *args, **kwargs)

        except falcon.json.decoder.JSONDecodeError:
            resp.status = falcon.HTTP_400
            resp.body = '{"error": "JSON is invalid"}'
            resp.content_type = "application/json; charset=UTF-8"

    return decorate


def json_body_resp(func):
    def decorate(self, req, resp, *args, **kwargs):
        f = func(self, req, resp, *args, **kwargs)
        resp.body = falcon.json.dumps(resp.body)
        resp.content_type = "application/json; charset=UTF-8"
        return f

    return decorate
